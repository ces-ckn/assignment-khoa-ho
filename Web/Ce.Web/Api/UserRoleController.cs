using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;

namespace Ce.Api.Controllers
{

    /// <summary>
    /// UserRole Api Controller
    /// </summary>
    [RoutePrefix("api/UserRole")]
    [Authorize(Roles = "Administrator")]
    public partial class UserRoleController : BaseApiController
    {
        #region fields
        private readonly IUserRoleService userroleService;
        #endregion

        #region constructors

        public UserRoleController(IUserRoleService userroleService)
        {
            this.userroleService = userroleService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] UserRoleCriteria criteria)
        {
            int totalRecords = 0;
            var result = userroleService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetUserRoleById/{id}", Name = "GetUserRoleById")]
        public HttpResponseMessage GetUserRoleById([FromUri] Guid id)
        {
            var data = userroleService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllUserRole")]
        public HttpResponseMessage GetAllUserRole()
        {
            var data = userroleService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] UserRoleViewModel viewModel)
        {

		     var result = userroleService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(UserRoleViewModel viewModel)
        {

            var result = userroleService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = userroleService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
