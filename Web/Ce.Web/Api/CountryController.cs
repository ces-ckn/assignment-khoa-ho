using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// Country Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/Country")]
    [Authorize(Roles = "Administrator")]
    public partial class CountryController : BaseApiController
    {
        #region fields
        private readonly ICountryService countryService;
        #endregion

        #region constructors

        public CountryController(ICountryService countryService)
        {
            this.countryService = countryService;
        }

        #endregion


        #region public actions

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] CountryCriteria criteria)
        {
            int totalRecords = 0;
            var result = countryService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetCountryById/{id}", Name = "GetCountryById")]
        public HttpResponseMessage GetCountryById([FromUri] Guid id)
        {
            var data = countryService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllCountry")]
        public HttpResponseMessage GetAllCountry()
        {
            var data = countryService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] CountryViewModel viewModel)
        {
            viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
            var result = countryService.Add(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


        [HttpPut]
        public HttpResponseMessage Update(CountryViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            var result = countryService.Update(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = countryService.Remove(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
