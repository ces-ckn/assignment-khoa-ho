using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System.Linq;
using Fw.Core.Extensions;
using Ce.Infrastructure.Domain;
using System.Collections.Generic;
using Ce.Web;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace Ce.Api.Controllers
{

    /// <summary>
    /// User Api Controller
    /// </summary>
    [RoutePrefix("api/User")]
    [Authorize(Roles = "Administrator")]
    public partial class UserController : BaseApiController
    {
        #region fields
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;
        private readonly IRoleService roleService;
        private ApplicationUserManager _userManager;
        #endregion

        #region constructors

        public UserController(IUserService userService, IUserRoleService userRoleService, IRoleService roleService)
        {
            this.userService = userService;
            this.userRoleService = userRoleService;
            this.roleService = roleService;
        }

        #endregion


        #region public actions

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] UserCriteria criteria)
        {
            int totalRecords = 0;
            var result = userService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetUserById/{id}", Name = "GetUserById")]
        public HttpResponseMessage GetUserById([FromUri] string id)
        {
            var data = userService.GetById(id);
            var rl = userRoleService.GetAll().Where(t => t.UserId == id);
            var roles = UserManager.GetRoles(id);
            roles.ForEach(t =>
            {
                data.Roles.Add(t);
            });
            //data.Roles = new List<UserRole>();
            //rl.ForEach(t =>
            //{
            //    //var role = roleService.Get(t.RoleId);
            //    var rvm = AutoMapper.Mapper.Map<UserRoleViewModel, UserRole>(t);
            //    data.Roles.Add(rvm);
            //});
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllUser")]
        public HttpResponseMessage GetAllUser()
        {
            var data = userService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public async Task<HttpResponseMessage> Create([FromBody] UserViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = viewModel.Email, Email = viewModel.Email };
                var result = await UserManager.CreateAsync(user, viewModel.Password);

                if (result.Succeeded)
                {
                    var newUser = userService.GetAll().Where(t => t.Email == viewModel.Email).FirstOrDefault();
                    if (newUser != null)
                    {

                        var callbackUrl = "www.ce-testing.com/confirm";
                        await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");


                        var data = userService.Add(viewModel);
                        return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
                    }
                }
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, false);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpPut]
        public HttpResponseMessage Update(UserViewModel viewModel)
        {

            var result = userService.Update(viewModel);
            var roles = roleService.GetAll().Select(t=>t.Name).ToArray();;
            UserManager.RemoveFromRoles(viewModel.Id.ToString(), roles);
            UserManager.AddToRoles(viewModel.Id.ToString(), viewModel.Roles.ToArray());
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = userService.Remove(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
