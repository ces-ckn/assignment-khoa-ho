using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;
using System.Linq;
using Ce.Web.Models;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// Beer Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/Beer")]
    [Authorize]
    public partial class BeerController : BaseApiController
    {
        #region fields
        private readonly IBeerService beerService;
        private readonly IUserService userService;
        #endregion

        #region constructors

        public BeerController(IBeerService beerService, IUserService userService)
        {
            this.beerService = beerService;
            this.userService = userService;
        }

        #endregion


        #region public actions

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] BeerCriteria criteria)
        {
            int totalRecords = 0;
            var result = beerService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetBeerById/{id}", Name = "GetBeerById")]
        public HttpResponseMessage GetBeerById([FromUri] Guid id)
        {

            var data = beerService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }


        [HttpGet]
        [Route("GetBeerByCustomer/{email}", Name = "GetBeerByCustomer")]
        public HttpResponseMessage GetBeerByCustomer([FromUri] string email)
        {

            var data = beerService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllBeer")]
        [AllowAnonymous]
        public HttpResponseMessage GetAllBeer()
        {
            var data = beerService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetCustomerBeers")]
        public HttpResponseMessage GetCustomerBeers()
        {
            var user = User.Identity.Name;
            var data = beerService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }



        [HttpPost]
        [Route("AddPassport"), ActionName("AddPassport")]
        public HttpResponseMessage AddPassport([FromBody] AddPassportViewModel vm)
        {
            var user = userService.GetAll().Where(t => t.Email == User.Identity.Name).FirstOrDefault();
            if (user != null)
            {
                var result = beerService.AddPassport(vm.BeerId, user.Id.ToString());

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.MethodNotAllowed, false);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] BeerViewModel viewModel)
        {
            viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
            var result = beerService.Add(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


        [HttpPut]
        public HttpResponseMessage Update(BeerViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            viewModel.Id = Guid.Parse(viewModel.Id.ToString());
            viewModel.BeerCategory = null;
            viewModel.Manufacturer = null;
            var result = beerService.Update(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = beerService.Remove(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
