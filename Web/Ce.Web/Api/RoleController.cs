using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;

namespace Ce.Api.Controllers
{

    /// <summary>
    /// Role Api Controller
    /// </summary>
    [RoutePrefix("api/Role")]
    [Authorize(Roles = "Administrator")]
    public partial class RoleController : BaseApiController
    {
        #region fields
        private readonly IRoleService roleService;
        #endregion

        #region constructors

        public RoleController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

        #endregion


        #region public actions

        [HttpPost]        
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] RoleCriteria criteria)
        {
            int totalRecords = 0;
            var result = roleService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetRoleById/{id}", Name = "GetRoleById")]
        public HttpResponseMessage GetRoleById([FromUri] string id)
        {
            var data = roleService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllRole")]
        public HttpResponseMessage GetAllRole()
        {
            var data = roleService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] RoleViewModel viewModel)
        {

		     var result = roleService.Add(viewModel);
             return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


		[HttpPut]
        public HttpResponseMessage Update(RoleViewModel viewModel)
        {

            var result = roleService.Update(viewModel);            
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = roleService.Remove(id);
			return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
