using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using System;
using System.Net.Http;
using System.Web.Http;
using Ce.Api.Controllers;
using System.Linq;

namespace Bw.Api.Controllers
{

    /// <summary>
    /// BeerCategory Api Controller
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    [RoutePrefix("api/BeerCategory")]
    [Authorize(Roles = "Administrator")]
    public partial class BeerCategoryController : BaseApiController
    {
        #region fields
        private readonly IBeerCategoryService beercategoryService;
        private readonly IBeerService beerService;
        #endregion

        #region constructors

        public BeerCategoryController(IBeerCategoryService beercategoryService, IBeerService beerService)
        {
            this.beercategoryService = beercategoryService;
            this.beerService = beerService;
        }

        #endregion


        #region public actions

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] BeerCategoryCriteria criteria)
        {
            int totalRecords = 0;
            var result = beercategoryService.Search(criteria, out totalRecords);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetBeerCategoryById/{id}", Name = "GetBeerCategoryById")]
        public HttpResponseMessage GetBeerCategoryById([FromUri] Guid id)
        {
            var data = beercategoryService.GetById(id);           
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllBeerCategory")]
        public HttpResponseMessage GetAllBeerCategory()
        {
            var data = beercategoryService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] BeerCategoryViewModel viewModel)
        {
            viewModel.CreatedBy = User.Identity.Name;
            viewModel.CreatedDate = DateTime.Now;
            var result = beercategoryService.Add(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


        [HttpPut]
        public HttpResponseMessage Update(BeerCategoryViewModel viewModel)
        {
            viewModel.UpdatedBy = User.Identity.Name;
            viewModel.UpdatedDate = DateTime.Now;
            viewModel.Id = Guid.Parse(viewModel.Id.ToString());
            var result = beercategoryService.Update(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = beercategoryService.Remove(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
