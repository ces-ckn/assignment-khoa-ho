using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Ce.Infrastructure.Criteria;
using Ce.Infrastructure.Services;
using Ce.Infrastructure.ViewModel;
using Ce.Web;
using Fw.Core.Extensions;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;


namespace Ce.Api.Controllers
{

    /// <summary>
    /// Customer Api Controller
    /// </summary>
    [RoutePrefix("api/Customer")]
    [Authorize(Roles = "Administrator")]
    public partial class CustomerController : BaseApiController
    {
        #region fields
        private readonly ICustomerService customerService;
        private ApplicationUserManager _userManager;
        #endregion

        #region constructors

        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #endregion


        #region public actions

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search([FromBody] UserCriteria criteria)
        {
            int totalRecords = 0;
            var result = customerService.Search(criteria, out totalRecords);
            result.ForEach(t =>
            {
                var roles = UserManager.GetRoles(t.Id.ToString());
                t.Roles = new List<string>();
                roles.ForEach(x=>{
                    t.Roles.Add(x);
                });
            });
            result = result.Where(t=>t.Roles.Contains("Customer"));
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, new { Data = result, TotalRecords = totalRecords });

        }


        [HttpGet]
        [Route("GetCustomerById/{id}", Name = "GetCustomerById")]
        public HttpResponseMessage GetCustomerById([FromUri] Guid id)
        {
            var data = customerService.GetById(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("GetAllCustomer")]
        public HttpResponseMessage GetAllCustomer()
        {
            var data = customerService.GetAll();
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("Create"), ActionName("Create")]
        public HttpResponseMessage Create([FromBody] UserViewModel viewModel)
        {
            viewModel.Roles = new List<string>();
            viewModel.Roles.Add("Customer");
            var result = customerService.Add(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


        [HttpPut]
        public HttpResponseMessage Update(UserViewModel viewModel)
        {

            var result = customerService.Update(viewModel);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public HttpResponseMessage Delete(Guid id)
        {
            var result = customerService.Remove(id);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, true);
        }

        #endregion

    }
}
