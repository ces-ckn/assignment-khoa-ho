﻿using System.Web;
using System.Web.Optimization;

namespace Ce.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                            "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            bundles.Add(new ScriptBundle("~/bundles/ckeditor",
                        @"//cdn.ckeditor.com/4.5.3/full/ckeditor.js"
                        ).Include(
                         "~/Scripts/ckeditor/ckeditor.js"));



            bundles.Add(new StyleBundle("~/Content/lte").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/loading-bar.css",
                      "~/Content/lte.min.css",
                      "~/Content/cropper.min.css",
                      "~/Content/angular-block-ui.css",
                      "~/Content/css/select2.css",
                      "~/Content/ng-table.css",
                      "~/Content/treeGrid.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                    "~/Scripts/angular.js",
                    "~/Scripts/angular-local-storage.min.js",
                    "~/Scripts/angular-ui/ui-bootstrap.js",
                    "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                    "~/Scripts/angularUI/ui-router.js",
                    "~/Scripts/angular-ui-router.js",
                    "~/Scripts/ng-table.js",
                    "~/Scripts/select2.js",
                    "~/Scripts/common.js",
                    "~/Scripts/cropper.js"
                    ));


        }
    }
}

