[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Ce.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Ce.Web.App_Start.NinjectWebCommon), "Stop")]

namespace Ce.Web.App_Start
{

    using System;
    using System.Web;
    using CommonServiceLocator.NinjectAdapter;
    using Microsoft.Practices.ServiceLocation;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject.Activation;
    using Fw.Core.DataContext;
    using Fw.Core.Repository;
    using Fw.Core.UnitOfWork;
    using Ce.Infrastructure.DatabaseContext;
    using Ce.Infrastructure.Services;
    public static class NinjectWebCommon
    {
        private static readonly Ninject.Web.Common.Bootstrapper bootstrapper = new Ninject.Web.Common.Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(Ninject.Web.Common.OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(Ninject.Web.Common.NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static Ninject.IKernel CreateKernel()
        {

            var kernel = new Ninject.StandardKernel();
            try
            {
                kernel.Bind<Func<Ninject.IKernel>>().ToMethod(ctx => () => new Ninject.Web.Common.Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<Ninject.Web.Common.HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);


                var locator = new NinjectServiceLocator(kernel);

                ServiceLocator.SetLocatorProvider(() => locator);


                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(Ninject.IKernel kernel)
        {

            kernel.Bind<IDataContext>().To<CeDbContext>()
                .InSingletonScope()
                .Named("Ce.Infrastructure.DatabaseContext")
                .WithConstructorArgument("_db", new CeDbContext());


            kernel.Bind(typeof(IRepository<>)).To(typeof(Repository<>));


            kernel.Bind<IUnitOfWork>().To<UnitOfWork>()
                .InSingletonScope();


            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IRoleService>().To<RoleService>();
            kernel.Bind<IUserRoleService>().To<UserRoleService>();


            kernel.Bind<ICountryService>().To<CountryService>();
            kernel.Bind<IManufacturerService>().To<ManufacturerService>();
            kernel.Bind<IBeerCategoryService>().To<BeerCategoryService>();
            kernel.Bind<IBeerService>().To<BeerService>();
            kernel.Bind<ICustomerService>().To<CustomerService>();


        }

    }

}
