﻿using System;
using Ce.Web.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(Ce.Web.Startup))]
namespace Ce.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            ConfigureOAuth(app); 
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            //app.CreatePerOwinContext(UserDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            var PublicClientId = "self";
            //use a cookie to temporarily store information about a user logging in with a thir
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(7),
                Provider = new ApplicationOAuthProvider(PublicClientId)
            };
            app.UseOAuthBearerTokens(OAuthServerOptions);
        }
    }
}
