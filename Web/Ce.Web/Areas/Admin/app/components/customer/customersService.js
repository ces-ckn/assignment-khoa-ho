'use strict';

define(['app'], function (app) {

	var injectParams = ['$http', '$q'];

	var customersFactory = function ($http, $q) {
	    var serviceBase = 'api/customer';
		var factory = {};

		factory.create = function (obj) {
			return $http.post(serviceBase + "/Create", obj).then(function (results) {
				obj.Id = results.Id;
				return results.Data;
			});
		}

		factory.update = function (obj) {
			return $http.put(serviceBase + "/Update", obj).then(function (results) {
				obj.Id = results.Id;
				return results.data;
			});
		}


		factory.delete = function (customerId) {
			return $http.delete(serviceBase + '/Delete/'+customerId).then(function (status) {
				return status;
			});
		}

		factory.getById = function (id) {
		    return $http.get(serviceBase + '/GetCustomerById/' + id).then(function (results) {
				return results.data;
			});
		}

		factory.getAll = function () {
			return $http.get(serviceBase + '/GetAllcustomer').then(function (results) {
				return results;
			});
		}


		factory.search = function (customerCriteria) {

			return $http.post(serviceBase + "/Search", customerCriteria).then(function (response) {
				return response;
			});
		};


		return factory;

	}

	customersFactory.$inject = injectParams;
	app.factory('customersService', customersFactory);

});
