'use strict';

define(['app'], function (app) {
	app.controller('CustomerItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'customersService', 'modalService', function ($scope, $location, $stateParams/*[@DependenciesService2]*/

						, $timeout, customersService, modalService) {

		var vm = this,
			customerId = ($stateParams.customerId) ? $stateParams.customerId : '',
			timer,
			onRouteChangeOff;

		vm.customer = {};
		// for customer Only		
		vm.formTitle = customerId == 0 ? 'Create new Customer' : 'Update Customer';		
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.savecustomer = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.customer.Id) {
					customersService.create(vm.customer).then(processSuccess, processError);
				}
				else {
					customersService.update(vm.customer).then(processSuccess, processError);
				}
				$location.path('/customers');
			}
		};

		vm.delete = function (customerId) {
			var headerText = 'customer';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete customer',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete customer?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					customersService.delete(customerId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/customers');
					}, processError);
				}
			});
		};

		function init() {

			if (customerId != '0') {
			    customersService.getById(customerId).then(function (data) {
					vm.customer = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the customer selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.customerId = 0;
				vm.customer = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/customers');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

