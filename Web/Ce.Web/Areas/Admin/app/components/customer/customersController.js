'use strict';

define(['app'], function (app) {

app.controller('CustomersController', ['$scope', '$filter', '$location', 'NgTableParams',
                    '$timeout', 'customersService', 'modalService', function ($scope, $filter, $location, NgTableParams,
    $timeout, customersService, modalService) {

    var vm = this;
    vm.customers = [];
    vm.currentPage = 1;
    vm.itemPerPage = 10;
    vm.totalRecords = 0;
    vm.errorMessage = "";
    vm.sortDirection = "asc";
    vm.columnName = "name";

    vm.searchText = "";

	initalData();

    function processError(error) {
        vm.errorMessage = error.message;
        App.alert({
            message: error.data.Message,
            type: 'danger'
        });
    };


    vm.searchTextChanged = function () {
        doSearch();
    };


    function initalData() {
		vm.tableParams = new NgTableParams({
				page: 1,
				count: 10,
				filter: {
					ModelNum: ''
				},
				sorting:
				{
					ModelNum: 'asc'
				}
			},
			{
				getData: function ($defer, params) {
					var criteria = {
						CurrentPage: params.page() - 1,
						ItemPerPage: params.count(),
						SortColumn: vm.columnName,
						SortDirection: vm.sortDirection,
						Name: vm.searchText
					};

					customersService.search(criteria).then(function (response) {

						vm.customers = response.data.Data;
						var orderedData = params.sorting() ?
							$filter('orderBy')(vm.customers, params.orderBy())
											: vm.customers;
						params.total(response.data.TotalRecords);
						$defer.resolve(orderedData);

					});


				}
			}, function (error) {
				console.log('errror', error);
			});
    }

	$scope.$on('doSearch', function (event, areaId) {
		initalData();
    });

    vm.edit = function (id) {
        $location.path('/customer/' + id);
    };

    vm.delete = function (id) {

        var delObj = getCustomerById(id);
        var objName = delObj.Name == undefined ? 'customer' : delObj.Name;
		modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                customersService.delete(id).then(function () {
                    for (var i = 0; i < vm.customers.length; i++) {
                        if (vm.customers[i].Id === id) {
                            vm.customers.splice(i, 1);
							vm.tableParams.reload();
                            break;
                        }
                    }

                }, function (error) {
                    App.alert({
                        message: error.data.Message,
                        type: 'danger'
                    });
                });
        });
    };

    function getCustomerById(id) {
        for (var i = 0; i < vm.customers.length; i++) {
            var delObj = vm.customers[i];
            if (delObj.Id === id) {
                return delObj;
            }
        }
        return null;
    }    

    function doSearch() {

        vm.tableParams.reload();
    }

}]);

});

