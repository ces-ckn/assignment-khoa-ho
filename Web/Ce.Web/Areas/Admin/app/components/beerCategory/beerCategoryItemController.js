'use strict';

define(['app'], function (app) {
	app.controller('BeerCategoryItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'beerCategoriesService', 'modalService',function ($scope, $location, $stateParams/*[@DependenciesService2]*/						

						, $timeout, beerCategoriesService, modalService) {

		var vm = this,
			beerCategoryId = ($stateParams.beerCategoryId) ? $stateParams.beerCategoryId : '',
			timer,
			onRouteChangeOff;

		vm.beerCategory = {};
		// for beerCategory Only		
		vm.formTitle = beerCategoryId == 0 ? 'Create new beerCategory' : 'Update beerCategory';
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.saveBeerCategory = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.beerCategory.Id) {
					beerCategoriesService.create(vm.beerCategory).then(processSuccess, processError);
				}
				else {
					beerCategoriesService.update(vm.beerCategory).then(processSuccess, processError);
				}
				$location.path('/beercategories');
			}
		};

		vm.delete = function (beerCategoryId) {
			var headerText = 'beerCategory';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete BeerCategory',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete beerCategory?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					beerCategoriesService.delete(beerCategoryId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/beerCategories');
					}, processError);
				}
			});
		};

		function init() {

			if (beerCategoryId != '0') {
				beerCategoriesService.getById(beerCategoryId).then(function (data) {
					vm.beerCategory = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the user selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.beerCategoryId = 0;
				vm.beerCategory = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/beercategories');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

