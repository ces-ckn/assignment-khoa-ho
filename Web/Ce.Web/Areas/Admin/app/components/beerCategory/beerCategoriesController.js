'use strict';

define(['app'], function (app) {

    app.controller('BeerCategoriesController', ['$scope', '$filter', '$location', 'NgTableParams',
                        '$timeout', 'beerCategoriesService', 'modalService', function ($scope, $filter, $location, NgTableParams,
        $timeout, beerCategoriesService, modalService) {

                            var vm = this;
                            vm.beerCategories = [];
                            vm.currentPage = 1;
                            vm.itemPerPage = 10;
                            vm.totalRecords = 0;
                            vm.errorMessage = "";
                            vm.sortDirection = "asc";
                            vm.columnName = "name";

                            vm.searchText = "";

                            initalData();

                            function processError(error) {
                                vm.errorMessage = error.message;
                                App.alert({
                                    message: error.data.Message,
                                    type: 'danger'
                                });
                            };


                            vm.searchTextChanged = function () {
                                doSearch();
                            };


                            function initalData() {
                                vm.tableParams = new NgTableParams({
                                    page: 1,
                                    count: 10,
                                    filter: {
                                        ModelNum: ''
                                    },
                                    sorting:
                                    {
                                        ModelNum: 'asc'
                                    }
                                },
                                    {
                                        getData: function ($defer, params) {
                                            var criteria = {
                                                CurrentPage: params.page() - 1,
                                                ItemPerPage: params.count(),
                                                SortColumn: vm.columnName,
                                                SortDirection: vm.sortDirection,
                                                Name: vm.searchText
                                            };

                                            beerCategoriesService.search(criteria).then(function (response) {

                                                vm.beerCategories = response.data.Data;
                                                var orderedData = params.sorting() ?
                                                    $filter('orderBy')(vm.beerCategories, params.orderBy())
                                                                    : vm.beerCategories;
                                                params.total(response.data.TotalRecords);
                                                $defer.resolve(orderedData);

                                            });


                                        }
                                    }, function (error) {
                                        console.log('errror', error);
                                    });
                            }

                            $scope.$on('doSearch', function (event, areaId) {
                                initalData();
                            });

                            vm.edit = function (id) {
                                $location.path('/beercategory/' + id);
                            };

                            vm.delete = function (id) {

                                var delObj = getBeerCategoryById(id);
                                var objName = delObj.Name == undefined ? 'beerCategory' : delObj.Name;
                                debugger
                                if (delObj.Beers.length > 0) {
                                    alert('This category is being used');
                                }
                                else {
                                    modalService.showMessageConfirm("Do you want to delete " + objName + " ?", function () {
                                        beerCategoriesService.delete(id).then(function () {
                                            for (var i = 0; i < vm.beerCategories.length; i++) {
                                                if (vm.beerCategories[i].Id === id) {
                                                    vm.beerCategories.splice(i, 1);
                                                    vm.tableParams.reload();
                                                    break;
                                                }
                                            }

                                        }, function (error) {
                                            App.alert({
                                                message: error.data.Message,
                                                type: 'danger'
                                            });
                                        });
                                    });
                                }
                            };

                            function getBeerCategoryById(id) {
                                for (var i = 0; i < vm.beerCategories.length; i++) {
                                    var delObj = vm.beerCategories[i];
                                    if (delObj.Id === id) {
                                        return delObj;
                                    }
                                }
                                return null;
                            }

                            function doSearch() {

                                vm.tableParams.reload();
                            }

                        }]);

});

