'use strict';

define(['app'], function (app) {
    app.controller('BeerItemController', ['$scope', '$location', '$stateParams', 'beerCategoriesService', 'manufacturersService'
						, '$timeout', 'beersService', 'modalService', function ($scope, $location, $stateParams, beerCategoriesService, manufacturersService

						, $timeout, beersService, modalService) {

						    var vm = this,
                                beerId = ($stateParams.beerId) ? $stateParams.beerId : '',
                                timer,
                                onRouteChangeOff;

						    vm.beer = {};
						    // for beer Only		
						    vm.formTitle = beerId == 0 ? 'Create new beer' : 'Update beer';
						    vm.isRepeat = true;

						    /*get[@GetParentTables]*/
						    beerCategoriesService.getAll().then(function (response) {
						        vm.categories = response.data;
						    }, processError);
						    manufacturersService.getAll().then(function (response) {
						        vm.manufacturers = response.data;
						    }, processError);

						    vm.saveBeer = function () {
						        if ($scope.itemForm.$valid) {
						            if (!vm.beer.Id) {
						                beersService.create(vm.beer).then(processSuccess, processError);
						            }
						            else {
						                beersService.update(vm.beer).then(processSuccess, processError);
						            }
						            $location.path('/beers');
						        }
						    };

						    vm.delete = function (beerId) {
						        var headerText = 'beer';
						        var modalOptions = {
						            closeButtonText: 'Cancel',
						            actionButtonText: 'Delete Beer',
						            headerText: 'Delete ' + headerText + '?',
						            bodyText: 'Do you want to delete beer?'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                beersService.delete(beerId).then(function () {
						                    onRouteChangeOff(); //Stop listening for location changes
						                    $location.path('/beers');
						                }, processError);
						            }
						        });
						    };

						    function init() {

						        if (beerId != '0') {
						            beersService.getById(beerId).then(function (data) {
						                vm.beer = angular.copy(data);
						            }, processError);
						        }

						        /*get[@CallGetParentTables]*/


						        //Make sure they're warned if they made a change but didn't save it
						        //Call to $on returns a "deregistration" function that can be called to
						        //remove the listener (see routeChange() for an example of using it)
						        onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

						    }

						    init();

						    function routeChange(event, newUrl, oldUrl) {
						        //Navigate to newUrl if the form isn't dirty
						        if (!vm.itemForm || !vm.itemForm.$dirty) return;

						        var modalOptions = {
						            closeButtonText: 'Close',
						            actionButtonText: 'Cancel',
						            headerText: 'Data has changed!',
						            bodyText: 'The data has changed, do you want to exist??'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                onRouteChangeOff(); //Stop listening for location changes
						                $location.path($location.url(newUrl).hash()); //Go to page they're interested in
						            }
						        });

						        //prevent navigation by default since we'll handle it
						        //once the user selects a dialog option
						        event.preventDefault();
						        return;
						    }

						    function processSuccess() {
						        ShowMessage({
						            message: 'Success.'
						        });
						        if (vm.isRepeat) {
						            vm.beerId = 0;
						            vm.beer = null;
						            ClearTextFields();
						        }
						        else {
						            $location.path('/beers');
						        }
						    }

						    function processError(error) {
						        ShowMessage({
						            message: error.data.Message,
						            type: 'danger'
						        });
						    }
						}]);

});

