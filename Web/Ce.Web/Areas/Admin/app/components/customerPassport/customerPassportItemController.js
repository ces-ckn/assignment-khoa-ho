'use strict';

define(['app'], function (app) {
	app.controller('CustomerPassportItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'customerPassportsService', 'modalService',function ($scope, $location, $stateParams/*[@DependenciesService2]*/						

						, $timeout, customerPassportsService, modalService) {

		var vm = this,
			customerPassportId = ($stateParams.customerPassportId) ? $stateParams.customerPassportId : '',
			timer,
			onRouteChangeOff;

		vm.customerPassport = {};
		// for customerPassport Only		
		vm.formTitle = customerPassportId == 0 ? 'Create customerPassport new' : 'Update customerPassport';		
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.saveCustomerPassport = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.customerPassport.Id) {
					customerPassportsService.create(vm.customerPassport).then(processSuccess, processError);
				}
				else {
					customerPassportsService.update(vm.customerPassport).then(processSuccess, processError);
				}
				$location.path('/customerPassports');
			}
		};

		vm.delete = function (customerPassportId) {
			var headerText = 'customerPassport';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete CustomerPassport',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete customerPassport?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					customerPassportsService.delete(customerPassportId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/customerPassports');
					}, processError);
				}
			});
		};

		function init() {

			if (customerPassportId != '0') {
				customerPassportsService.getById(customerPassportId).then(function (data) {
					vm.customerPassport = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the user selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.customerPassportId = 0;
				vm.customerPassport = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/customerPassports');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

