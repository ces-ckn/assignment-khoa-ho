'use strict';

define(['app'], function (app) {
	app.controller('CountryItemController', ['$scope', '$location', '$stateParams'/*[@DependenciesService1]*/

						, '$timeout', 'countriesService', 'modalService',function ($scope, $location, $stateParams/*[@DependenciesService2]*/						

						, $timeout, countriesService, modalService) {

		var vm = this,
			countryId = ($stateParams.countryId) ? $stateParams.countryId : '',
			timer,
			onRouteChangeOff;

		vm.country = {};
		// for country Only		
		vm.formTitle = countryId == 0 ? 'Create country new' : 'Update country';		
		vm.isRepeat = true;

		/*get[@GetParentTables]*/	


		vm.saveCountry = function () {
			if ($scope.itemForm.$valid) {
				if (!vm.country.Id) {
					countriesService.create(vm.country).then(processSuccess, processError);
				}
				else {
					countriesService.update(vm.country).then(processSuccess, processError);
				}
				$location.path('/countries');
			}
		};

		vm.delete = function (countryId) {
			var headerText = 'country';
			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Delete Country',
				headerText: 'Delete ' + headerText + '?',
				bodyText: 'Do you want to delete country?'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					countriesService.delete(countryId).then(function () {
						onRouteChangeOff(); //Stop listening for location changes
						$location.path('/countries');
					}, processError);
				}
			});
		};

		function init() {

			if (countryId != '0') {
				countriesService.getById(countryId).then(function (data) {
					vm.country = angular.copy(data);
				}, processError);
			}

			/*get[@CallGetParentTables]*/


			//Make sure they're warned if they made a change but didn't save it
			//Call to $on returns a "deregistration" function that can be called to
			//remove the listener (see routeChange() for an example of using it)
			onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

		}

		init();

		function routeChange(event, newUrl, oldUrl) {
			//Navigate to newUrl if the form isn't dirty
			if (!vm.itemForm || !vm.itemForm.$dirty) return;

			var modalOptions = {
				closeButtonText: 'Close',
				actionButtonText: 'Cancel',
				headerText: 'Data has changed!',
				bodyText: 'The data has changed, do you want to exist??'
			};

			modalService.showModal({}, modalOptions).then(function (result) {
				if (result === 'ok') {
					onRouteChangeOff(); //Stop listening for location changes
					$location.path($location.url(newUrl).hash()); //Go to page they're interested in
				}
			});

			//prevent navigation by default since we'll handle it
			//once the user selects a dialog option
			event.preventDefault();
			return;
		}

		function processSuccess() {
			ShowMessage({
				message: 'Success.'
			});
			if (vm.isRepeat) {
				vm.countryId = 0;
				vm.country = null;
				ClearTextFields();
			}
			else
			{
				$location.path('/countries');
			}
		}

		function processError(error) {
			ShowMessage({
				message: error.data.Message,
				type: 'danger'
			});
		}
	}]);

});

