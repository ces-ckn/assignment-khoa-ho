'use strict';

define(['app'], function (app) {
    app.controller('UserItemController', ['$scope', '$location', '$stateParams', 'rolesService'

						, '$timeout', 'usersService', 'modalService', function ($scope, $location, $stateParams, rolesService

						, $timeout, usersService, modalService) {

						    var vm = this,
                                userId = ($stateParams.userId) ? $stateParams.userId : '',
                                timer,
                                onRouteChangeOff;

						    vm.user = {};
						    // for user Only		
						    vm.formTitle = userId == 0 ? 'Create user new' : 'Update user';
						    vm.isRepeat = true;

						    rolesService.getAll().then(function (response) {
						        vm.roles = response.data;
						    }, processError);


						    vm.saveUser = function () {
						        if ($scope.itemForm.$valid) {

						            if (!vm.user.Id) {
						                usersService.create(vm.user).then(processSuccess, processError);
						            }
						            else {
						                usersService.update(vm.user).then(processSuccess, processError);
						            }
						            $location.path('/users');
						        }
						    };

						    vm.delete = function (userId) {
						        var headerText = 'user';
						        var modalOptions = {
						            closeButtonText: 'Cancel',
						            actionButtonText: 'Delete User',
						            headerText: 'Delete ' + headerText + '?',
						            bodyText: 'Do you want to delete user?'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                usersService.delete(userId).then(function () {
						                    onRouteChangeOff(); //Stop listening for location changes
						                    $location.path('/users');
						                }, processError);
						            }
						        });
						    };

						    function init() {

						        if (userId != '0') {
						            usersService.getById(userId).then(function (data) {
						                vm.user = angular.copy(data);
						                vm.user.$new = false;
						            }, processError);
						        }
						        else {
						            vm.user.$new = true;
						        }

						        /*get[@CallGetParentTables]*/


						        //Make sure they're warned if they made a change but didn't save it
						        //Call to $on returns a "deregistration" function that can be called to
						        //remove the listener (see routeChange() for an example of using it)
						        onRouteChangeOff = $scope.$on('$locationChangeStart', routeChange);

						    }

						    init();

						    function routeChange(event, newUrl, oldUrl) {
						        //Navigate to newUrl if the form isn't dirty
						        if (!vm.itemForm || !vm.itemForm.$dirty) return;

						        var modalOptions = {
						            closeButtonText: 'Close',
						            actionButtonText: 'Cancel',
						            headerText: 'Data has changed!',
						            bodyText: 'The data has changed, do you want to exist??'
						        };

						        modalService.showModal({}, modalOptions).then(function (result) {
						            if (result === 'ok') {
						                onRouteChangeOff(); //Stop listening for location changes
						                $location.path($location.url(newUrl).hash()); //Go to page they're interested in
						            }
						        });

						        //prevent navigation by default since we'll handle it
						        //once the user selects a dialog option
						        event.preventDefault();
						        return;
						    }

						    function processSuccess() {
						        ShowMessage({
						            message: 'Success.'
						        });
						        if (vm.isRepeat) {
						            vm.userId = 0;
						            vm.user = null;
						            ClearTextFields();
						        }
						        else {
						            $location.path('/users');
						        }
						    }

						    function processError(error) {
						        ShowMessage({
						            message: error.data.Message,
						            type: 'danger'
						        });
						    }
						}]);

});

