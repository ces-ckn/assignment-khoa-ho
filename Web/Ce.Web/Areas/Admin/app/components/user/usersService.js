'use strict';

define(['app'], function (app) {

    var injectParams = ['$http', '$q'];

    var UsersFactory = function ($http, $q) {
        var serviceBase = 'api/user';
        var factory = {};

        factory.create = function (obj) {
            //var para = { data: JSON.stringify(obj) };
            return $http.post(serviceBase + "/Create", obj).then(function (results) {
                obj.Id = results.Id;
                return results.Data;
            });
            //$.ajax({
            //    type: 'POST',
            //    url: '/api/user/Create',
            //    contentType: 'application/json; charset=utf-8',
            //    data: JSON.stringify(obj)
            //}).done(function (data) {
            //    return data;
            //}).fail(function () { });
        }

        factory.update = function (obj) {
            return $http.put(serviceBase + "/Update", obj).then(function (results) {
                obj.Id = results.Id;
                return results.data;
            });
        }


        factory.delete = function (userId) {
            return $http.delete(serviceBase + '/Delete/' + userId).then(function (status) {
                return status;
            });
        }

        factory.getById = function (id) {
            return $http.get(serviceBase + '/GetUserById/' + id).then(function (results) {
                return results.data;
            });
        }

        factory.getAll = function () {
            return $http.get(serviceBase + '/GetAllUser').then(function (results) {
                return results;
            });
        }


        factory.search = function (userCriteria) {

            return $http.post(serviceBase + "/Search", userCriteria).then(function (response) {
                return response;
            });
        };


        return factory;

    }

    UsersFactory.$inject = injectParams;
    app.factory('usersService', UsersFactory);

});
