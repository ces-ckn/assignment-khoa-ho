﻿require.config({
    baseUrl: '/Areas/Admin/app',
    urlArgs: 'v=1.0',
    waitSeconds: 200
});


require(
    [
        , 'app'
        , '/Scripts/ng-ckeditor.js'
        , '/Scripts/tree-grid-directive.js'         
        , 'shared/directives/checklistModel'
        , 'shared/services/globalService'

        ////, 'app.components.min'
        , 'components/country/countriesService'
        , 'components/country/countriesController'
        , 'components/country/countryItemController'

        , 'components/manufacturer/manufacturersService'
        , 'components/manufacturer/manufacturersController'
        , 'components/manufacturer/manufacturerItemController'

        , 'components/beercategory/beercategoriesService'
        , 'components/beercategory/beercategoriesController'
        , 'components/beercategory/beercategoryItemController'

        , 'components/beer/beersService'
        , 'components/beer/beersController'
        , 'components/beer/beerItemController'

        , 'components/customer/customersService'
        , 'components/customer/customersController'
        , 'components/customer/customerItemController'


        , 'components/user/usersService'
        , 'components/user/usersController'
        , 'components/user/userItemController'

        , 'components/role/rolesService'
        , 'components/role/rolesController'
        , 'components/role/roleItemController'

        , 'components/home/homeController'


    ],

    function () {
        angular.bootstrap(document, ['lvApp']);
    });


