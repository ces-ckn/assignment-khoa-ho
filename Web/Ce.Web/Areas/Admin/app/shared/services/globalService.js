﻿'use strict';

define(['app'], function (app) {
    var injectParams = ['$http', '$q', 'localStorageService'];
    app.factory('globalService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
        var serviceBase = 'api/upload';
        var factory = {};

        factory.upload = function (data) {
            return $http.post(serviceBase + '/UploadImage', data).then(function (results) {
                return results;
            });
        }

        factory.deleteImageTemp = function (module) {

            return $http.post(serviceBase + '/DeleteAllImageTemp/' + module).then(function (status) {
                return status;
            });
        }


        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }
        factory.guid = function () {
            // then to call it, plus stitch in '4' in the third group
            var value = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
            return value;
        }

        return factory;

    }])
});