﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['carCategoriesService'];

    var checkunique = function (carCategoriesService) {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {

                var cachedValue = "";
                var isFirstWatch = true;

                scope.$watch('form.$valid', function (validity) {
                    scope.modalInvalid = !validity;
                })

                scope.$watch(function () {
                    if (isFirstWatch) {
                        cachedValue = elem.val();
                        isFirstWatch = false;
                    }
                    return elem.val();
                }, function (value) {
                });

                elem.on('blur', function (event) {
                    scope.$apply(function () {
                        var value = elem.val();
                        var typeForCheckUnique = attrs.name;
                        if (typeForCheckUnique == 'carCategory') {
                            carCategoriesService.checkUniqueCarCategory(value).then
                            (function (response) {
                                ctrl.$setValidity('checkunique', (response.length == 0) || (cachedValue == value));
                                scope.modalInvalid = scope.form.$invalid;
                            }, scope.processError);
                        }
                    });
                });
            }
        };
    }
    checkunique.$inject = injectParams;

    app.directive('checkunique', checkunique);
});
