﻿'use strict';

define(['Areas/Admin/app/top/appSettings.js'
    , 'Areas/Admin/app/top/authService.js'
    , 'Areas/Admin/app/top/imgService.js'], function () {
        var app = angular.module('lvApp', ['ui.router', 'ui.bootstrap', 'lv.authentication', 'lv.Settings'
            , 'ngTable', 'LocalStorageModule','treeGrid']);

        app.config(['$stateProvider', '$urlRouterProvider','treegridTemplateProvider',
                function ($stateProvider, $urlRouterProvider, treegridTemplateProvider) {

                    $stateProvider
                    .state('admin', {
                        url: '/admin',
                        templateUrl: '/Areas/Admin/app/components/home/index.html'
                         , resolve: {
                         }
                        , controller: 'HomeController as vm'
                    })                    
                     .state('beercategories', {
                         url: '/beercategories',
                         templateUrl: '/Areas/Admin/app/components/beercategory/beercategories.html'
                        , resolve: {
                        }
                        , controller: 'BeerCategoriesController as vm'
                     })
                     .state('beercategory', {
                         url: '/beercategory/:beerCategoryId',
                         templateUrl: '/Areas/Admin/app/components/beercategory/beercategory.html'
                        , resolve: {
                        }
                        , controller: 'BeerCategoryItemController as vm'
                     })
                     .state('beers', {
                         url: '/beers',
                         templateUrl: '/Areas/Admin/app/components/beer/beers.html'
                        , resolve: {
                        }
                        , controller: 'BeersController as vm'
                     })
                     .state('beer', {
                         url: '/beer/:beerId',
                         templateUrl: '/Areas/Admin/app/components/beer/beer.html'
                        , resolve: {
                        }
                        , controller: 'BeerItemController as vm'
                     })
                     .state('manufacturers', {
                         url: '/manufacturers',
                         templateUrl: '/Areas/Admin/app/components/manufacturer/manufacturers.html'
                        , resolve: {
                        }
                        , controller: 'ManufacturersController as vm'
                     })
                     .state('manufacturer', {
                         url: '/manufacturer/:manufacturerId',
                         templateUrl: '/Areas/Admin/app/components/manufacturer/manufacturer.html'
                        , resolve: {
                        }
                        , controller: 'ManufacturerItemController as vm'
                     })
                     .state('countries', {
                         url: '/countries',
                         templateUrl: '/Areas/Admin/app/components/country/countries.html'
                        , resolve: {
                        }
                        , controller: 'CountriesController as vm'
                     })
                     .state('country', {
                         url: '/country/:countryId',
                         templateUrl: '/Areas/Admin/app/components/country/country.html'
                        , resolve: {
                        }
                        , controller: 'CountryItemController as vm'
                     })
                     .state('customers', {
                         url: '/customers',
                         templateUrl: '/Areas/Admin/app/components/customer/customers.html'
                        , resolve: {
                        }
                        , controller: 'CustomersController as vm'
                     })
                     .state('customer', {
                         url: '/customer/:customerId',
                         templateUrl: '/Areas/Admin/app/components/customer/customer.html'
                        , resolve: {
                        }
                        , controller: 'CustomerItemController as vm'
                     })

                     .state('user', {
                         url: '/user/:userId',
                         templateUrl: '/Areas/Admin/app/components/user/user.html'
                        , resolve: {
                        }
                        , controller: 'UserItemController as vm'
                     })
                    .state('users', {
                        url: '/users',
                        templateUrl: '/Areas/Admin/app/components/user/users.html'
                        , resolve: {
                        }
                        , controller: 'UsersController as vm'
                    })
                     .state('role', {
                         url: '/role/:roleId',
                         templateUrl: '/Areas/Admin/app/components/role/role.html'
                        , resolve: {
                        }
                        , controller: 'RoleItemController as vm'
                     })
                    .state('roles', {
                        url: '/roles',
                        templateUrl: '/Areas/Admin/app/components/role/roles.html'
                        , resolve: {
                        }
                        , controller: 'RolesController as vm'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: '/Areas/Admin/app/shared/views/login.html'
                        , resolve: {
                        }
                        //, controller: 'LoginController as vm'
                    })

                    $urlRouterProvider.otherwise('/admin');
                }]);



        app.config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
        });


        app.run(['$rootScope', '$location', 'authService',
            function ($rootScope, $location, authService) {
                authService.fillAuthData();
                $rootScope.$on('$stateChangeStart',
                    function (event, toState, toParams, fromState, fromParams) {
                        //alert('Changed');
                        // event.preventDefault();
                        // transitionTo() promise will be rejected with 
                        // a 'transition prevented' error
                    });
            }]);

        app.directive('ckEditor', [function () {
            return {
                require: '?ngModel',
                link: function ($scope, elm, attr, ngModel) {
                    
                    var ck = CKEDITOR.replace(elm[0]);

                    ck.on('pasteState', function () {
                        $scope.$apply(function () {
                            ngModel.$setViewValue(ck.getData());
                        });
                    });

                    ngModel.$render = function (value) {
                        ck.setData(ngModel.$modelValue);
                    };
                }
            };
        }])

        

        return app;
    });
