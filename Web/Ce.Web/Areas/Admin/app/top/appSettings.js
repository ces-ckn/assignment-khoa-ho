﻿
'use strict';

define([], function () {

    var serviceBase = 'http://localhost:12568/';
    //var serviceBase = 'http://ce-demo.logviet.com/';

    var appSetting = angular.module('lv.Settings', [])

    var injectParams = ['$uibModal'];
    //var injectParams = [];

    var modalService = function ($uibModal) {


        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: '/Areas/Admin/app/shared/partials/modalConfirm.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Has changes',
            bodyText: 'Perform this action?'
        };



        this.openModal = function (modalPath, customModalOptions) {

            var modal = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: modalPath
            };

            return this.show(modal, customModalOptions);
        };


        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };



        this.alertInfor = function (message) {
            var customModalOptions = {
                templateUrl: '/Areas/Admin/app/shared/partials/modalInfo.html',
                controller: function ($scope, $uibModalInstance) {
                    $scope.alertText = message;
                    $scope.closeModal = function () {
                        $uibModalInstance.close('dismiss');
                    }
                }
            };
            $uibModal.open(customModalOptions);
        }

        this.alertError = function (message) {
            var customModalOptions = {
                actionButtonText: 'OK',
                bodyText: message
            };
            var customModalDefaults = {
                templateUrl: '/Areas/Admin/app/shared/partials/modalError.html'
            }
            this.showModal(customModalDefaults, customModalOptions);
        }

        this.showMessageConfirm = function (message, okFunction, cancelFuntion) {

            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'OK',
                headerText: 'Confirm',
                bodyText: message
            };

            this.showModal({}, modalOptions).then(function (result) {
                if (result === 'ok') {
                    if (okFunction) {
                        okFunction();
                    }

                }
                else {
                    if (cancelFuntion) {
                        cancelFuntion();
                    }
                }
            });
        }

        this.show = function (customModalDefaults, customModalOptions) {


            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in this service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in this service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $uibModalInstance) {


                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $uibModalInstance.close('ok');
                    };
                    $scope.modalOptions.close = function (result) {
                        $uibModalInstance.close('cancel');
                    };
                };

                tempModalDefaults.controller.$inject = ['$scope', '$uibModalInstance'];
            }

            return $uibModal.open(tempModalDefaults).result;
        };

        this.showPopup = function (controller, view, width, height, data, callBackFunction) {
            var customModalOptions = {
                backdrop: 'static',
                animation: true,
                templateUrl: view,
                controller: controller,
                scope: data,
                resolve: {

                }
            };

            var modalInstance = $uibModal.open(customModalOptions);

            //modalInstance.opened.then(function () {
            //    debugger;
            //    if ($(".modal-dialog").length > 0) {
            //        debugger;
            //    }
            //});

            modalInstance.result.then(function (data) {
                callBackFunction(data);
            }, function () {
            });

            setTimeout(function () {
                if (width != undefined && width != null) {
                    $(".modal-dialog").css("width", width);
                }

            }, 500);

        }

    };

    modalService.$inject = injectParams;

    appSetting.service('modalService', modalService);

    appSetting.constant('ngSetting', {
        apiServiceBaseUri: serviceBase,
        clientId: 'lvApp'
    });

    appSetting.constant('ENUMS',
        {
            LocationList: [{ Id: 0, Name: 'Top' }, { Id: 1, Name: 'Left' }, { Id: 2, Name: 'Right' }, { Id: 3, Name: 'Bottom' }]
            , LocationArray: { 0: 'Top', 1: 'Right', 2: 'Left', 3: 'Bottom' }

        });
});