﻿using System.Web.Mvc;

namespace Ce.Web.Controllers
{
    [Authorize(Roles ="Customer")]
    public class CustomerController : BaseController
    {
        public CustomerController()
        {
            
        }

        public ActionResult Index()
        {
            ViewBag.Message = "CE-Demo.";
            return View();
        }

    }
}