﻿using System.Web.Mvc;

namespace Ce.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        public HomeController()
        {
            
        }

        public ActionResult Index()
        {
            ViewBag.Message = "CE-Demo.";
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "CE-Demo.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


    }
}