using Ce.Infrastructure.ViewModel;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Fw.Core.Service;
using Ce.Infrastructure.Domain;
using Fw.Core.UnitOfWork;
using System.Data.Entity;

namespace Ce.Infrastructure.Services
{

    /// <summary>
    /// BeerCategory service interface
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public interface IBeerCategoryService : IService<BeerCategory, BeerCategoryViewModel>
    {
        IEnumerable<BeerCategoryViewModel> Search(BeerCategoryCriteria criteria, out int totalRecords);
    }

    /// <summary>
    /// BeerCategory service
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public partial class BeerCategoryService : Service<BeerCategory, BeerCategoryViewModel>, IBeerCategoryService
    {

        #region constructors

        public BeerCategoryService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #endregion

        #region public methods

        public IEnumerable<BeerCategoryViewModel> Search(BeerCategoryCriteria criteria, out int totalRecords)
        {

            Expression<Func<BeerCategory, bool>> predicate = t =>
                        ((criteria.Id == null || criteria.Id == Guid.Empty || t.Id == criteria.Id.Value)
                            && (criteria.Status == null || t.Status == criteria.Status.Value)
                            && (string.IsNullOrEmpty(criteria.Name) || (t.Name.Contains(criteria.Name) || criteria.Name.Contains(t.Name)))
                        )
                            ;

            var query = unitOfWork.Repository<BeerCategory>().Find(predicate).Include(t => t.Beers);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

            switch (criteria.SortColumn)
            {
                case "createdby":
                    query = isAsc ? query.OrderBy(t => t.CreatedBy) : query.OrderByDescending(t => t.CreatedBy);
                    break;
                case "updatedby":
                    query = isAsc ? query.OrderBy(t => t.UpdatedBy) : query.OrderByDescending(t => t.UpdatedBy);
                    break;
                case "createddate":
                    query = isAsc ? query.OrderBy(t => t.CreatedDate) : query.OrderByDescending(t => t.CreatedDate);
                    break;
                case "updateddate":
                    query = isAsc ? query.OrderBy(t => t.UpdatedDate) : query.OrderByDescending(t => t.UpdatedDate);
                    break;
                case "name":
                    query = isAsc ? query.OrderBy(t => t.Name) : query.OrderByDescending(t => t.Name);
                    break;
                default: break;
            }

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<BeerCategory, BeerCategoryViewModel>(t)).ToList();
        }


        #endregion
    }
}
