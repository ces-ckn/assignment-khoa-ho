using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Ce.Infrastructure.Services
{

    /// <summary>
    /// Beer service interface
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public interface IBeerService : IService<Beer, BeerViewModel>
    {
        IEnumerable<BeerViewModel> Search(BeerCriteria criteria, out int totalRecords);
        bool AddPassport(Guid beerId, string email);
    }

    /// <summary>
    /// Beer service
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public partial class BeerService : Service<Beer, BeerViewModel>, IBeerService
    {

        #region constructors

        public BeerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #endregion

        #region public methods

        public IEnumerable<BeerViewModel> Search(BeerCriteria criteria, out int totalRecords)
        {
            lock (unitOfWork)
            {
                Expression<Func<Beer, bool>> predicate = t =>
                            ((criteria.Id == null || criteria.Id == Guid.Empty || t.Id == criteria.Id.Value)
                                && (criteria.BeerCategoryId == null || criteria.BeerCategoryId == Guid.Empty || t.BeerCategoryId == criteria.BeerCategoryId.Value)
                                && (string.IsNullOrEmpty(criteria.Name) || (t.Name.Contains(criteria.Name) || criteria.Name.Contains(t.Name)))
                                && (criteria.IsArchived == null || t.IsArchived == criteria.IsArchived.Value)
                                && (criteria.Status == null || t.Status == criteria.Status.Value)
                                && (criteria.ManufacturerId == null || criteria.ManufacturerId == Guid.Empty || t.ManufacturerId == criteria.ManufacturerId.Value)
                            )
                                ;

                var query = unitOfWork.Repository<Beer>()
                    .Find(predicate)
                    .Include(t => t.BeerCategory);

                totalRecords = query.Count();

                criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
                bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

                switch (criteria.SortColumn)
                {
                    case "name":
                        query = isAsc ? query.OrderBy(t => t.Name) : query.OrderByDescending(t => t.Name);
                        break;
                    case "description":
                        query = isAsc ? query.OrderBy(t => t.Description) : query.OrderByDescending(t => t.Description);
                        break;
                    case "createdby":
                        query = isAsc ? query.OrderBy(t => t.CreatedBy) : query.OrderByDescending(t => t.CreatedBy);
                        break;
                    case "updatedby":
                        query = isAsc ? query.OrderBy(t => t.UpdatedBy) : query.OrderByDescending(t => t.UpdatedBy);
                        break;
                    case "createddate":
                        query = isAsc ? query.OrderBy(t => t.CreatedDate) : query.OrderByDescending(t => t.CreatedDate);
                        break;
                    case "updateddate":
                        query = isAsc ? query.OrderBy(t => t.UpdatedDate) : query.OrderByDescending(t => t.UpdatedDate);
                        break;
                    default: break;
                }

                query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

                return query.ToList().Select(t => AutoMapper.Mapper.Map<Beer, BeerViewModel>(t)).ToList();
            }
        }

        public override Beer Get(object id)
        {
            Expression<Func<Beer, bool>> predicate = t =>
                       (t.Id.Equals(id));

            var obj = repository.Find(predicate).Include(t => t.BeerCategory);
            return obj.FirstOrDefault();
        }

        public bool AddPassport(Guid beerId, string customerId)
        {
            var existed = unitOfWork.Repository<CustomerPassport>()
                                    .GetAll()
                                    .Any(t => t.CustomerId == customerId && t.BeerId == beerId);
            if (!existed)
            {
                var newCP = new CustomerPassport()
                {
                    Id = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    BeerId = beerId,
                    CustomerId = customerId
                };
                unitOfWork.Repository<CustomerPassport>().Add(newCP);
                var result = unitOfWork.Complete() > 0;
                return result;
            }
            return false;
        }

        public override IEnumerable<BeerViewModel> GetAll()
        {
            var result = repository.GetAll()
                                    .Include(t => t.BeerCategory)
                                    .Include(t => t.CustomerPassports)
                                    .Where(t => !t.IsArchived)
                                    .ToList();

            var data = result.Select(t => AutoMapper.Mapper.Map<Beer, BeerViewModel>(t)).ToList();
            //.Select(t => AutoMapper.Mapper.Map<Beer, BeerViewModel>(t));
            return data.ToList();
        }

        #endregion
    }
}
