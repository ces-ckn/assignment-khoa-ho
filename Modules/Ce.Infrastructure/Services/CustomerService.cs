using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Fw.Core.DataContext;
using Ce.Infrastructure.DatabaseContext;
using System.Data.Entity;

namespace Ce.Infrastructure.Services
{

    /// <summary>
    /// User service interface
    /// </summary>
    public interface ICustomerService : IService<User, UserViewModel>
    {
        bool AddPassport(Guid beerCateoryId);
        IEnumerable<UserViewModel> Search(UserCriteria criteria, out int totalRecords);
    }

    /// <summary>
    /// User service
    /// </summary>
    public partial class CustomerService : Service<User, UserViewModel>, ICustomerService
    {

        #region constructors

        public CustomerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion

        #region public methods

        public IEnumerable<UserViewModel> Search(UserCriteria criteria, out int totalRecords)
        {

            Expression<Func<User, bool>> predicate = t =>
                        ((string.IsNullOrEmpty(criteria.Id) || (t.Id.Contains(criteria.Id) || criteria.Id.Contains(t.Id)))
                            && (string.IsNullOrEmpty(criteria.Email) || (t.Email.Contains(criteria.Email) || criteria.Email.Contains(t.Email)))
                            && (string.IsNullOrEmpty(criteria.UserName) || (t.UserName.Contains(criteria.UserName) || criteria.UserName.Contains(t.UserName)))
                        )
                            ;

            var query = unitOfWork.Repository<User>().Find(predicate)
                            .Include(t => t.Roles);
                            //.Where(t => t.Roles.Where(x => x.RoleId == "ce12fe3a-32a5-43a4-909e-b3c30aa98514").Count() > 0);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

            switch (criteria.SortColumn)
            {
                case "name":
                case "email":
                    query = isAsc ? query.OrderBy(t => t.Email) : query.OrderByDescending(t => t.Email);
                    break;

                case "username":
                    query = isAsc ? query.OrderBy(t => t.UserName) : query.OrderByDescending(t => t.UserName);
                    break;
                default: break;
            }

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<User, UserViewModel>(t)).ToList();
        }


        #endregion

        public bool AddPassport(Guid beerCateoryId)
        {
            throw new NotImplementedException();
        }


    }
}
