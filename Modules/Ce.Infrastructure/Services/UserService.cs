using Ce.Infrastructure.Domain;
using Ce.Infrastructure.ViewModel;
using Fw.Core.Service;
using Fw.Core.UnitOfWork;
using System.Collections.Generic;
using System;
using Ce.Infrastructure.Criteria;
using System.Linq;
using System.Linq.Expressions;
using Fw.Core.DataContext;
using Ce.Infrastructure.DatabaseContext;
using System.Data.Entity;
using Fw.Core.Extensions;


namespace Ce.Infrastructure.Services
{

    /// <summary>
    /// User service interface
    /// </summary>
    public interface IUserService : IService<User, UserViewModel>
    {
        IEnumerable<UserViewModel> Search(UserCriteria criteria, out int totalRecords);
    }

    /// <summary>
    /// User service
    /// </summary>
    public partial class UserService : Service<User, UserViewModel>, IUserService
    {

        #region constructors

        public UserService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        #endregion

        #region public methods

        public IEnumerable<UserViewModel> Search(UserCriteria criteria, out int totalRecords)
        {

            Expression<Func<User, bool>> predicate = t =>
                        ((string.IsNullOrEmpty(criteria.Id) || (t.Id.Contains(criteria.Id) || criteria.Id.Contains(t.Id)))
                            && (string.IsNullOrEmpty(criteria.Email) || (t.Email.Contains(criteria.Email) || criteria.Email.Contains(t.Email)))
                            && (criteria.EmailConfirmed == null || t.EmailConfirmed == criteria.EmailConfirmed.Value)
                            && (string.IsNullOrEmpty(criteria.PhoneNumber) || (t.PhoneNumber.Contains(criteria.PhoneNumber) || criteria.PhoneNumber.Contains(t.PhoneNumber)))
                            && (criteria.PhoneNumberConfirmed == null || t.PhoneNumberConfirmed == criteria.PhoneNumberConfirmed.Value)
                            && (criteria.LockoutEnabled == null || t.LockoutEnabled == criteria.LockoutEnabled.Value)
                            && (criteria.AccessFailedCount == null || t.AccessFailedCount.Equals(criteria.AccessFailedCount.Value))
                            && (string.IsNullOrEmpty(criteria.UserName) || (t.UserName.Contains(criteria.UserName) || criteria.UserName.Contains(t.UserName)))
                        )
                            ;

            var query = unitOfWork.Repository<User>().Find(predicate);

            totalRecords = query.Count();

            criteria.SortColumn = string.IsNullOrEmpty(criteria.SortColumn) ? string.Empty : criteria.SortColumn.ToLower();
            bool isAsc = criteria.SortDirection.ToLower().Equals("asc");

            switch (criteria.SortColumn)
            {
                case "email":
                case "name":
                    query = isAsc ? query.OrderBy(t => t.Email) : query.OrderByDescending(t => t.Email);
                    break;
                default: break;
            }

            query = query.Skip(criteria.CurrentPage * criteria.ItemPerPage).Take(criteria.ItemPerPage);

            return query.ToList().Select(t => AutoMapper.Mapper.Map<User, UserViewModel>(t)).ToList();
        }

        public override User Get(object id)
        {
            Expression<Func<User, bool>> predicate = t =>
                       (t.Id.ToString() == id.ToString());

            var obj = repository.Find(predicate).Include(t => t.Roles);
            return obj.FirstOrDefault();
        }

        #endregion
    }
}
