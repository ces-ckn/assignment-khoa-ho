﻿using System;
using System.Data.Entity;
using Ce.Infrastructure.Domain;
using Fw.Core.Constants;
using Fw.Core.DataContext;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Ce.Infrastructure.DatabaseContext
{
    public class CeDbContext : IdentityDbContext<User>, IDataContext
    {


        public CeDbContext()
            : base(Constant.AppConnection)
        {

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;

            Database.Initialize(false);

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<BeerCategory> BeerCategories { get; set; }
        public DbSet<Beer> Beers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("CE");
            modelBuilder.Entity<IdentityUser>()
               .ToTable("Users");
            modelBuilder.Entity<User>()
                .ToTable("Users");

            modelBuilder.Entity<IdentityRole>()
            .ToTable("Roles");

            modelBuilder.Entity<IdentityUserRole>()
                .ToTable("UserRoles");

            modelBuilder.Entity<IdentityUserClaim>()
                .ToTable("UserClaims");

            modelBuilder.Entity<IdentityUserLogin>()
                .ToTable("UserLogins");
            
        }

        public static CeDbContext Create()
        {
            return new CeDbContext();
        }

      
    }
}
