﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fw.Core;

namespace Ce.Infrastructure.Domain
{
    public class Beer : Entity
    {
        public Guid BeerCategoryId { get; set; }

        public Guid ManufacturerId { get; set; }

        public string Name { get; set; }
        public bool IsArchived { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        [ForeignKey("BeerCategoryId")]
        public BeerCategory BeerCategory { get; set; }

        [ForeignKey("ManufacturerId")]
        public Manufacturer Manufacturer { get; set; }

        public virtual ICollection<CustomerPassport> CustomerPassports { get; set; }
    }
}
