﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ce.Infrastructure.Domain
{
    public class Customer : User
    {

        public virtual ICollection<CustomerPassport> CustomerPassports { get; set; }
    }
}
