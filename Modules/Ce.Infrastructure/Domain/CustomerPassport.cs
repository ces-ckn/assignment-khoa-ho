﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fw.Core;

namespace Ce.Infrastructure.Domain
{
    public class CustomerPassport : Entity
    {
        public string CustomerId { get; set; }

        public Guid BeerId { get; set; }


        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [ForeignKey("BeerId")]
        public Beer Beer { get; set; }

    }
}
