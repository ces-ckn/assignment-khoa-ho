using Fw.Core.ViewModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// User View Model
    /// </summary>
    public class UserViewModel : BaseViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Password { get; set; }

        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        
        public bool PhoneNumberConfirmed { get; set; }
        
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        
        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        
        public string Discriminator { get; set; }


        public ICollection<string> Roles { get; set; }
    }
}
