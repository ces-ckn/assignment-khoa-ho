using Fw.Core.ViewModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Ce.Infrastructure.ViewModel
{
    /// <summary>
    /// Role View Model
    /// </summary>
    public class RoleViewModel : BaseViewModel
    {

        [Required]
        public string Name { get; set; }
    }
}
