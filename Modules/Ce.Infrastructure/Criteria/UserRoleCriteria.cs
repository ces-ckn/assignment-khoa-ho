using Fw.Core.Criteria;
using System;

namespace Ce.Infrastructure.Criteria
{
    /// <summary>
    /// UserRole criteria
    /// </summary>
    public class UserRoleCriteria : BaseCriteria
    {
			public string UserId {get;set;}
			public string RoleId {get;set;}
			public string IdentityUser_Id {get;set;}
    }
}
