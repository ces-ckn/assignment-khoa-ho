using System;
using Fw.Core.Criteria;

namespace Ce.Infrastructure.Criteria
{
    /// <summary>
    /// Beer criteria
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public class BeerCriteria : BaseCriteria
    {
			public Guid? Id {get;set;}
			public Guid? BeerCategoryId {get;set;}
			public string Name {get;set;}
			public bool? IsArchived {get;set;}
			public decimal? Price {get;set;}
			public string Description {get;set;}
			public string CreatedBy {get;set;}
			public string UpdatedBy {get;set;}
			public DateTime? CreatedDate {get;set;}
			public DateTime? UpdatedDate {get;set;}
			public bool? Status {get;set;}
			public Guid? ManufacturerId {get;set;}
    }
}
