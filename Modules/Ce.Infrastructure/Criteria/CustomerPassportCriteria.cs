using System;
using Fw.Core.Criteria;

namespace Ce.Infrastructure.Criteria
{
    /// <summary>
    /// CustomerPassport criteria
    /// Created by KhoaHT
    /// Created Date: Friday, April 29, 2016
    /// </summary>
    public class CustomerPassportCriteria : BaseCriteria
    {
			public Guid? Id {get;set;}
			public string CustomerId {get;set;}
			public Guid? BeerCategoryId {get;set;}
			public string CreatedBy {get;set;}
			public string UpdatedBy {get;set;}
			public DateTime? CreatedDate {get;set;}
			public DateTime? UpdatedDate {get;set;}
			public bool? Status {get;set;}
    }
}
