﻿using System;
using Fw.Core.Repository;
using Fw.Core.DataContext;
using Fw.Core.Service;
using Microsoft.Practices.ServiceLocation;
using Ninject;
using System.Data.Entity;

namespace Fw.Core.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Private Fields

        /// <summary>
        /// The data context
        /// </summary>
        private IDataContext dataContext;


        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed;


        /// <summary>
        /// Gets the data context.
        /// </summary>
        /// <value>
        /// The data context.
        /// </value>
        public DbContext DataContext
        {
            get
            {
                return dataContext as DbContext;
            }

        }

        #endregion Private Fields

        #region Constuctor/Dispose


        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        public UnitOfWork(IDataContext context)
        {
            this.dataContext = context;
        }



        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        public virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

                // IDisposable only

                if (dataContext != null)
                {
                    dataContext.Dispose();
                    dataContext = null;
                }
            }

            // release any unmanaged objects
            // set the object references to null

            disposed = true;
        }

        #endregion Constuctor/Dispose



        /// <summary>
        /// Completes this instance.
        /// </summary>
        /// <returns></returns>
        public int Complete()
        {
            return dataContext.SaveChanges();
        }


        /// <summary>
        /// Repositories this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            //var kernel = new StandardKernel();
            //return kernel.Get<IRepository<TEntity>>();


            if (ServiceLocator.Current != null)
            {
                return ServiceLocator.Current.GetInstance<IRepository<TEntity>>();
            }

            return Repository<TEntity>();
        }
    }
}
