﻿using Fw.Core.Repository;
using System;
using System.Data.Entity;

namespace Fw.Core.UnitOfWork
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IUnitOfWork : IDisposable
    {

        /// <summary>
        /// Repositories this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;


        DbContext DataContext { get; }

        /// <summary>
        /// Completes this instance.
        /// </summary>
        /// <returns></returns>
        int Complete();
    }
}
