﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using DelegateDecompiler;

namespace Fw.Core.Extensions
{
    public static class MapperExtensions
    {
        public static List<TDestination>
            ToList<TDestination>(this IProjectionExpression projectionExpression)
        {
            return projectionExpression.To<TDestination>().Decompile().ToList();
        }
    }
}
