﻿using System;
using System.ComponentModel;

namespace Fw.Core.Criteria
{
    public class BaseCriteria
    {
        public Guid? Id { get; set; }
        [DefaultValue(1)]
        public int CurrentPage { get; set; }

        [DefaultValue(20)]
        public int ItemPerPage { get; set; }

        [DefaultValue("Name")]
        public string SortColumn { get; set; }


        [DefaultValue("asc")]
        public string SortDirection { get; set; }

        public string SearchText { get; set; }

    }
}
