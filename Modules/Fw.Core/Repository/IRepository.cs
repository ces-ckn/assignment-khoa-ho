﻿using Fw.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Fw.Core.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Add an Entity
        /// </summary>
        /// <param name="entity"></param>
        void Add(TEntity entity);


        void Update(TEntity entity);

        /// <summary>
        /// Add Entities Range
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Remove an Entity
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);

        /// <summary>
        /// Remove entities
        /// </summary>
        /// <param name="entities"></param>
        void RemoveRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Get by Id as Guid
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        TEntity GetById(object key);


        /// <summary>
        /// Get all entity
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Get entities with predication
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IQueryable<TEntity> Find(Expression<Func<TEntity,bool>> predicate);
    }
}
