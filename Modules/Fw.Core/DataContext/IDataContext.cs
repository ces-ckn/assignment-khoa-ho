﻿using System;
namespace Fw.Core.DataContext
{
    public interface IDataContext : IDisposable
    {
        int SaveChanges();
    }
}
